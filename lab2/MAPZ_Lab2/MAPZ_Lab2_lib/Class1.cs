﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MAPZ_Lab2_lib
{
    public static class ExtensionClass
    {
        public static string GetLeaders(this List<MyLab2.Legion> leg)
        {
            return string.Join(", ", leg.Select(a => a.Cent.LeaderCohort));
        }
    }
    public class MyLab2
    {
        static void Main(string[] args)
        {
            var Llegions = new List<Legion>(){
                new Legion(new Century("Augustus",60,3),new Cohort("Augustus",3),new Cavalry(12)),
                new Legion(new Century("Pompei",50,4),new Cohort("Pompei",4),new Cavalry(15)),
                new Legion(new Century("Cesar",80,1),new Cohort("Cesar",1),new Cavalry(11)),
                new Legion(new Century("Brutus",40,5),new Cohort("Brutus",5),new Cavalry(29)),
                new Legion(new Century("Causius",20,6),new Cohort("Causius",6),new Cavalry(30)),
                new Legion(new Century("Lebinus",70,2),new Cohort("Lebinus",2),new Cavalry(20))
            };
            var DLegions = new Dictionary<int, Legion>()
            {
                [1] = new Legion(new Century("Augustus", 60, 3), new Cohort("Augustus", 3), new Cavalry(12)),
                [2] = new Legion(new Century("Pompei", 50, 4), new Cohort("Pompei", 4), new Cavalry(15)),
                [3] = new Legion(new Century("Cesar", 80, 1), new Cohort("Cesar", 1), new Cavalry(11)),
                [4] = new Legion(new Century("Brutus", 40, 5), new Cohort("Brutus", 5), new Cavalry(29)),
                [5] = new Legion(new Century("Causius", 20, 6), new Cohort("Causius", 6), new Cavalry(30)),
                [6] = new Legion(new Century("Lebinus", 70, 2), new Cohort("Lebinus", 2), new Cavalry(20))
            };
            var result = Llegions.OrderBy(x => x.Cent.LeaderCohort);
            foreach (var x in result)
            {
                Console.WriteLine(x.Cent.LeaderCohort);
            }
            /*            Console.WriteLine(string.Join("\n", result));*/
        }
        public static List<string> SelectLeaders(List<Legion> Legions)
        {
            return Legions.Select(a => a.Cent.LeaderCohort).Distinct().ToList();
        }
        public static List<Legion> WhereCavSoldierHigher15(List<Legion> Legions)
        {
            return Legions.Where(x => x.Cav.Soldiers > 15).ToList();
        }
        public static List<KeyValuePair<int, Legion>> WhereKeysLess3(Dictionary<int, Legion> leg)
        {
            return leg.Where(eleme => eleme.Key < 4).ToList();
        }
        public static string AnonymousLegions(List<Legion> leg)
        {
            var temp = from x in leg select new { x.Cent.LeaderCohort };
            string result = "";
            foreach (var v in temp)
            {
                result += $"{v.LeaderCohort} ";
            }
            return result;
        }
        public class CompareLegion : IComparer<Legion>
        {
            public int Compare(Legion a, Legion b)
            {
                return a.Cent.Soldiers.CompareTo(b.Cent.Soldiers);
            }
        }
        public static List<Legion> SortByCentSoldiers(List<Legion> leg)
        {
            Legion[] result = leg.ToArray();
            Array.Sort(result, new CompareLegion());
            return result.ToList();
        }
        public class Century
        {
            public Century(string leadCoh, int sols, int cohid)
            {
                LeaderCohort = leadCoh;
                Soldiers = sols;
                CohortID = cohid;
            }
            public string LeaderCohort { get; set; }
            public int Soldiers { get; set; }
            public int CohortID { get; set; }
        }
        public class Cohort
        {
            public Cohort(string lead, int id)
            {
                Leader = lead;
                ID = id;
            }
            public string Leader { get; set; }
            public int ID { get; set; }
        }
        public class Cavalry
        {
            public Cavalry(int sols)
            {
                Soldiers = sols;
            }
            public int Soldiers { get; set; }
        }
        public class Legion
        {
            public Legion(Century cent, Cohort coh, Cavalry cav)
            {
                Cent = cent;
                Coh = coh;
                Cav = cav;
            }
            public Century Cent;
            public Cohort Coh;
            public Cavalry Cav;

        }
    }
}
