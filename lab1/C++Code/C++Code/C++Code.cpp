#include <iostream>
#include <chrono>
#include <iostream>
#include <ctime>
#include <iostream>
#include <chrono>

using namespace std;
class OverrideObject
{
public:
    void BasicOperation()
    {
        int* a = new int[1000];
    }
};
int main()
{
    OverrideObject* obj = new OverrideObject[100000];
    obj[0].BasicOperation();
    auto start = chrono::steady_clock::now();
    for (long i = 0; i < 100000; i++)
    {
        obj[i].BasicOperation();
    }
    auto end = chrono::steady_clock::now();
    std::cout << "RunTime " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << std::endl;
    start = chrono::steady_clock::now();
        delete []obj;
    end = chrono::steady_clock::now();
    std::cout << "Destroying " << chrono::duration_cast<chrono::milliseconds>(end - start).count() << std::endl;
}

