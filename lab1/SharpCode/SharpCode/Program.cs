﻿using System;
using System.Diagnostics;
namespace SharpCode
{
    class Program
    {
        enum MyEnum
        {
            First,
            Second,
            Third
        }
        public static void Ref(ref short x)
        {
            x = 9;
        }
        public static void Out(out short x)
        {
            x = 9;
        }
        class OverrideObject
        {
            public override bool Equals(Object obj)
            {
                return true;
            }
            public override int GetHashCode()
            {
                return 135;
            }
            public static new bool Equals(object obj1, object obj2)
            {
                return true;
            }
            public override String ToString()
            {
                return "Babadiboop";
            }
            public static new Object MemberwiseClone()
            {
                Object obj1=new Object();
                return obj1; 
            }
            public static new Type GetType() 
            {
                Type t=typeof(String);
                return t; 
            }
            public void BasicOperation()
            {
                int[] a = new int[1000];
            }
        }
        static void Main(string[] args)
        {
            /*           short a=0,b;
                       Ref(ref a);
                       Out(out b);
                       Console.WriteLine(a);
                       Console.WriteLine(b);
                       Console.WriteLine(MyEnum.First&MyEnum.Second);
           *//*            Console.WriteLine(MyEnum.First && MyEnum.Second);*//*
                       Console.WriteLine(MyEnum.First | MyEnum.Third);
           *//*            Console.WriteLine(MyEnum.First || MyEnum.Third);*//*
                       Console.WriteLine(MyEnum.Second ^ MyEnum.Third);
                       StaticDynamic obj = new StaticDynamic();
                       obj.Print();
                       short x = 1;
                       object o = x; //Забоксуємо
                       short x2 = (short)o; //Розбуксовуємо
                       obj = 3;
                       obj = (StaticDynamic)"Hello";*/
            OverrideObject []obj = new OverrideObject[100000];
            for (long i = 0; i < 100000; i++)
            {
                obj[i] = new OverrideObject();
            }
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (long i = 0; i < 100000; i++)
            {
                obj[i].BasicOperation();
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            stopWatch.Start();
            for (long i = 0; i < 100000; i++)
            {
                obj[i] = null;
            }
            stopWatch.Stop();
            ts = stopWatch.Elapsed;
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("Destroying " + elapsedTime);
        }
        struct MyStruct
        {
            short x;
            void y() { }
        }
        interface IInterfaceFirst
        {
            void Test();
        }
        interface IInterfaceSecond
        {
            void Testosteron();
        }
        class MultipleInherit : IInterfaceFirst, IInterfaceSecond
        {
            void IInterfaceFirst.Test ()
            {
                Console.WriteLine("Test This guy");
            }
            void IInterfaceSecond.Testosteron()
            {
                Console.WriteLine("mmmmm. Yeah. Test it");
            }
        }
        class ClassThird : AbstractSecond
        {
            private class TestingClass
            {
                private short a;
                protected short a1;
                public short a2;
            } 
            short x;
            void y() { }
            public override void AnotherTest()
            {
                Console.WriteLine("Yet another bababooe!");
            }
        }
        abstract class AbstractSecond:IInterfaceFirst
        {
            void IInterfaceFirst.Test()
            {
                Console.WriteLine("Bababooe!");
            }
            public abstract void AnotherTest();

            public void PublicMethod() { }
            private void PrivateMethod() { }
            protected void ProtectedMethod() { }
            internal void InternalMethod() { }
            internal protected void InternalProtMethod() { }
        }
        abstract class Parent
        {
            public Parent() { }
            public Parent(short x) { }
        }
        class Child:Parent
        {
            short smth;
            public Child(short x) 
            {
                this.smth = x;
            }
            public Child(short x, short y) : base(y)
            {
                this.smth = x;
            }
            public void Print(short smth)
            {
                Console.WriteLine(smth);
            }
            public void Print()
            {
                Console.WriteLine("Smtg");
            }
        }
        class StaticDynamic
        {
            static short x;
            short y;
            string str;
            public StaticDynamic()
            {
                x = 1;
                y = 2;
            }
            public StaticDynamic(short a)
            {
                x = a;
                y = x;
            }
            public StaticDynamic(string s)
            {
                str = s;
            }
            static StaticDynamic()
            {
                x = 0;
                /*                y = 3;*/
            }
            public void Print()
            {
                Console.WriteLine(x + " " + y);
            }
            public static implicit operator StaticDynamic(short a)
            {
                return new StaticDynamic(a);
            }
            public static explicit operator StaticDynamic(string s)
            {
                return new StaticDynamic(s);
            }

        }
    }
}
