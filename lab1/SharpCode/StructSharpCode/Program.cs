﻿using System;

namespace SharpCode
{
    struct Program
    {
        enum MyEnum
        {
            First,
            Second,
            Third
        }
        public static void Ref(ref short x)
        {
            x = 9;
        }
        public static void Out(out short x)
        {
            x = 9;
        }
        static void Main(string[] args)
        {
            short a = 0, b;
            Ref(ref a);
            Out(out b);
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(MyEnum.First & MyEnum.Second);
            /*            Console.WriteLine(MyEnum.First && MyEnum.Second);*/
            Console.WriteLine(MyEnum.First | MyEnum.Third);
            /*            Console.WriteLine(MyEnum.First || MyEnum.Third);*/
            Console.WriteLine(MyEnum.Second ^ MyEnum.Third);
            StaticDynamic obj = new StaticDynamic();
            obj.Print();
            short x = 1;
            object o = x; //Забоксуємо
            short x2 = (short)o; //Розбуксовуємо
            obj = 3;
            obj = (StaticDynamic)"Hello";
        }
        struct MyStruct
        {
            short x;
            void y() { }
        }
        interface IInterfaceFirst
        {
            void Test();
        }
        interface IInterfaceSecond
        {
            void Testosteron();
        }
        struct MultipleInherit : IInterfaceFirst, IInterfaceSecond
        {
            void IInterfaceFirst.Test()
            {
                Console.WriteLine("Test This guy");
            }
            void IInterfaceSecond.Testosteron()
            {
                Console.WriteLine("mmmmm. Yeah. Test it");
            }
        }
        struct structThird : AbstractSecond
        {
            private struct Testingstruct
            {
                private short a;
                protected short a1;
                public short a2;
            }
            short x;
            void y() { }
            public override void AnotherTest()
            {
                Console.WriteLine("Yet another bababooe!");
            }
        }
        abstract struct AbstractSecond : IInterfaceFirst
        {
            void IInterfaceFirst.Test()
            {
                Console.WriteLine("Bababooe!");
            }
            public abstract void AnotherTest();

            public void PublicMethod() { }
            private void PrivateMethod() { }
            protected void ProtectedMethod() { }
            internal void InternalMethod() { }
            internal protected void InternalProtMethod() { }
        }
        abstract struct Parent
        {
            public Parent() { }
            public Parent(short x) { }
        }
        struct Child : Parent
        {
            short smth;
            public Child(short x)
            {
                this.smth = x;
            }
            public Child(short x, short y) : base(y)
            {
                this.smth = x;
            }
            public void Print(short smth)
            {
                Console.WriteLine(smth);
            }
            public void Print()
            {
                Console.WriteLine("Smtg");
            }
        }
        struct StaticDynamic
        {
            static short x;
            short y;
            string str;
            public StaticDynamic()
            {
                x = 1;
                y = 2;
            }
            public StaticDynamic(short a)
            {
                x = a;
                y = x;
            }
            public StaticDynamic(string s)
            {
                str = s;
            }
            static StaticDynamic()
            {
                x = 0;
                /*                y = 3;*/
            }
            public void Print()
            {
                Console.WriteLine(x + " " + y);
            }
            public static implicit operator StaticDynamic(short a)
            {
                return new StaticDynamic(a);
            }
            public static explicit operator StaticDynamic(string s)
            {
                return new StaticDynamic(s);
            }

        }
    }
}
