﻿using System;

namespace MAPZ_Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player=Player.GetPlayer("Margaret,wdfwre1,34");
            player.Info();
            player = Player.GetPlayer("Jaremy,wertw,333");
            player.Info();

            ResourceFactory factory = new GoldResource();
            State state = factory.CreateState();
            state.War();

            LandBuilder mountains = new Mountains();
            Client client = new Client();
            client.Create(mountains);
            mountains.Land.Info();
        }
    }
    class Player
    {
        private static Player _instance;
        public static Player GetPlayer(string s)
        {
            if (_instance == null)
                _instance = new Player(s);
            return _instance;
        }
        static string username;
        static string password;
        static string gameCurrency;
        private Player(string s)
        {
            string[] arrs = s.Split(",");
            username = arrs[0];
            password = arrs[1];
            gameCurrency = arrs[2];
        }

        public void Info()
        {
            Console.WriteLine(username);
            Console.WriteLine(gameCurrency);
        }
    }
    abstract class State
    {
        public abstract void War();
    }
    class Gold:State
    {
        public override void War()
        {
            Console.WriteLine("Shoots you with a golden minigun, deals 20 damage!\nHas nothing to defend itself");
        }
    }
    class Wood : State
    {
        public override void War()
        {
            Console.WriteLine("Shoots you with a catapulta, deals 10 damage!\nDefends itself with a fortress");
        }
    }
    abstract class ResourceFactory
    {
        public abstract State CreateState();
    }
    class GoldResource: ResourceFactory
    {
        public override State CreateState()
        {
            return new Gold();
        }
    }
    class WoodResource : ResourceFactory
    {
        public override State CreateState()
        {
            return new Wood();
        }
    }
    class Land
    {
        public int Gold { get; set; }
        public int Wood { get; set; }
        public int Rock { get; set; }
        public void Info()
        {
            Console.WriteLine($"Gold is {Gold}, Wood is {Wood}, Rock is {Rock}");
        }
    }
    abstract class LandBuilder
    {
        protected Land land;
        public Land Land{get{ return land; }}
        public abstract void BuildResourses();
    }
    class Client
    {
        public void Create(LandBuilder landb)
        {
            landb.BuildResourses();
        }
    }
    class Desert : LandBuilder
    {
        public Desert()
        {
            land = new Land();
        }
        public override void BuildResourses()
        {
            land.Gold = 230;
            land.Rock = 50;
            land.Wood = 45;
        }
    }
    class Mountains : LandBuilder
    {
        public Mountains()
        {
            land = new Land();
        }
        public override void BuildResourses()
        {
            land.Gold = 30;
            land.Rock = 480;
            land.Wood = 130;
        }
    }
}
