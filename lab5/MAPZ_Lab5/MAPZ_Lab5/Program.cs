﻿using System;
using System.Collections.Generic;
namespace MAPZ_Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            Handler h1 = new LowerLevel();
            Handler h2 = new MediumLevel();
            Handler h3 = new HighLevel();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3);
            // Generate and process request
            int[] requests = { 2, 5, 14, 22, 18, 3, 27, 20 };
            foreach (int request in requests)
            {
                h1.PurgePrize(request);
            }

            DataCategory land = new LandType();
            land.Display(5);
            DataCategory weapons = new WeaponType();
            weapons.Display(3);

            EventList playereventlist = new EventList();
            playereventlist.Add("Samual","Desert");
            playereventlist.Add("Jimmy", "Mountain");
            playereventlist.Add("Sandra", "Desert");
            playereventlist.Add("Vivek", "Desert");
            playereventlist.Add("Anna", "Mountain");
            playereventlist.SetEventStrategy(new HurricaneEvent());
            playereventlist.Damage();
            playereventlist.SetEventStrategy(new EarthquakeEvent());
            playereventlist.Damage();
        }
        //CHAIN OF RESPONSIBILITY PATTERN
        public abstract class Handler
        {
            protected Handler successor;
            public void SetSuccessor(Handler successor)
            {
                this.successor = successor;
            }
            public abstract void PurgePrize(int level);
        }

        public class LowerLevel : Handler
        {
            public override void PurgePrize(int level)
            {
                if (level >= 0 && level < 10)
                {
                    Console.WriteLine("Congratulation! You survived the purge. " +
                        "Your level is {0} so you get 10% land increase", level);
                }
                else if (successor != null)
                {
                    successor.PurgePrize(level);
                }
            }
        }

        public class MediumLevel : Handler
        {
            public override void PurgePrize(int level)
            {
                if (level >= 10 && level < 20)
                {
                    Console.WriteLine("Congratulation! You survived the purge. " +
                        "Your level is {0} so you get 20% land increase", level);
                }
                else if (successor != null)
                {
                    successor.PurgePrize(level);
                }
            }
        }

        public class HighLevel : Handler
        {
            public override void PurgePrize(int level)
            {
                if (level >= 20 && level < 30)
                {
                    Console.WriteLine("Congratulation! You survived the purge. " +
                        "Your level is {0} so you get 30% land increase", level);
                }
                else if (successor != null)
                {
                    successor.PurgePrize(level);
                }
            }
        }

        //TEMPLATE PATTERN
        public abstract class DataCategory
        {
            public abstract void Connect();
            public abstract void Select();
            public abstract void Process(int top);
            public abstract void Disconnect();

            public void Display(int top)
            {
                Connect();
                Select();
                Process(top);
                Disconnect();
            }
        }
        public class LandType : DataCategory
        {
            private List<string> land;
            public override void Connect()
            {
                land = new List<string>();
            }
            public override void Select()
            {
                land.Add("Arctic");
                land.Add("Forest");
                land.Add("Desert");
                land.Add("Mountains");
                land.Add("Plain");
                land.Add("Jungle");
            }
            public override void Process(int top)
            {
                Console.WriteLine("Type of surface ---- ");
                for (int i = 0; i < top; i++)
                {
                    Console.WriteLine(land[i]);
                }

                Console.WriteLine();
            }
            public override void Disconnect()
            {
                land.Clear();
            }
        }

        public class WeaponType: DataCategory
        {
            private List<string> weapons;
            public override void Connect()
            {
                weapons = new List<string>();
            }
            public override void Select()
            {
                weapons.Add("Gun");
                weapons.Add("Javelin");
                weapons.Add("Catapulta");
                weapons.Add("Sword");
                weapons.Add("Bomb");
                weapons.Add("Bow");
            }
            public override void Process(int top)
            {
                Console.WriteLine("Type of weapon ---- ");
                for (int i = 0; i < top; i++)
                {
                    Console.WriteLine(weapons[i]);
                }
                Console.WriteLine();
            }
            public override void Disconnect()
            {
                weapons.Clear();
            }
        }

        //STRATEGY PATTERN
        public abstract class EventStrategy
        {
            public abstract void Damage(List<string> lname, List<string> lland, List<string> lresult);
        }

        public class HurricaneEvent : EventStrategy
        {
            public override void Damage(List<string> lname, List<string> lland, List<string> lresult)
            {
                for(int i = 0; i < lname.Count; i++)
                {
                    if (lland[i] == "Desert")
                    {
                        lresult[i] = "the damage is gonna be 30% health, destroyed infrastracture";
                    }else if (lland[i] == "Mountain")
                    {
                        lresult[i] = "the damage is gonna be 5% health, surviving infrastracture";
                    }
                }
                Console.WriteLine("Hurricane result: ");
            }
        }
        public class EarthquakeEvent : EventStrategy
        {
            public override void Damage(List<string> lname, List<string> lland, List<string> lresult)
            {
                for (int i = 0; i < lname.Count; i++)
                {
                    if (lland[i] == "Desert")
                    {
                        lresult[i] = "the damage is gonna be 10% health, tall buildings will fall";
                    }
                    else if (lland[i] == "Mountain")
                    {
                        lresult[i] = "the damage is gonna be 40% health, destroyed infrastracture";
                    }
                }
                Console.WriteLine("Earthquake result: ");
            }
        }
        public class EventList
        {
            private List<string> namelist = new List<string>();
            private List<string> landlist = new List<string>();
            private List<string> resultlist = new List<string>();
            private EventStrategy eventstrategy;
            public void SetEventStrategy(EventStrategy eventstrategy)
            {
                this.eventstrategy = eventstrategy;
            }
            public void Add(string name,string land)
            {
                namelist.Add(name);
                landlist.Add(land);
                resultlist.Add("");
            }
            public void Damage()
            {
                eventstrategy.Damage(namelist,landlist,resultlist);
                for(int i = 0; i < namelist.Count; i++) {
                    Console.WriteLine(" " + namelist[i] + " lives in " + landlist[i] + " so " +resultlist[i]);
                }
                Console.WriteLine();
            }
        }
    }
}
