﻿using System;

namespace MAPZ_Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            King k1 = new King("Arthur", 20, 30, 10);
            King k2 = new King("Seimur", 30, 20, 15);
            Battle battle=new Battle();
            battle.RunBattle(k1, k2);

            Gun gun = new Gun();
            GoldenGun goldenGun = new GoldenGun(gun);
            goldenGun.Damage();
            GunProxy gunpro = new GunProxy();
            gunpro.Damage();
        }
    }
    public class King
    {
        public string name;
        public int armor;
        public int power;
        public int land;
        public King(string name,int armor,int power, int land)
        {
            this.name = name;
            this.armor = armor;
            this.power = power;
            this.land = land;
        }
    }
    public class Land
    {
        public bool IsLand(King k)
        {
            bool check=true;
            if (k.land <= 0)
            {
                Console.WriteLine($"{k.name} has no land");
                check = false;
            }
            else
            {
                Console.WriteLine($"{k.name} has land");
            }
            return check;
        }
    }
    public class Power
    {
        public bool IsPower(King k)
        {
            bool check = true;
            if (k.power <= 0)
            {
                Console.WriteLine($"{k.name} has no power");
                check = false;
            }
            else
            {
                Console.WriteLine($"{k.name} has power");
            }
            return check;
        }
    }
    public class Fight
    {
        public bool IsAlive(King k1,King k2)
        {
            bool check = true;
            k2.armor = k2.armor - k1.power;
            if (k2.armor<0)
            {
                Console.WriteLine($"{k2.name} has died");
                k1.land += k2.land;
                check = false;
            }
            else
            {
                Console.WriteLine($"{k2.name} has survived");
            }
            return check;
        }
    }
    public class Battle
    {
        Land _Land= new Land();
        Power _Power = new Power();
        Fight _Fight = new Fight();
        public void RunBattle(King k1,King k2)
        {
            bool check=false;
            if (!_Land.IsLand(k1)) {
                check= true;
            }else if (!_Land.IsLand(k2)){
                check = true;
            }
            else if(!_Power.IsPower(k1)){
                check = true;
            }
            else if (!_Power.IsPower(k2)){
                check = true;
            }
            if (check)
            {
                Console.WriteLine("There is no battle!");
                return;
            }
            _Fight.IsAlive(k1, k2);
        }
    }
    public interface Weapon
    {
        int amount { get; }
        void Damage();
    }
    public class Gun : Weapon
    {
        public int amount{get { return 30; }}
        public void Damage()
        {
            int damage = amount * 60;
            Console.WriteLine($"The damage is {damage}");
        }
    }
    public abstract class WeaponDecorator : Weapon
    {
        private Weapon _Weapon;

        public WeaponDecorator(Weapon Weapon)
        {
            _Weapon = Weapon;
        }
        public int amount {get { return _Weapon.amount; }}
        public void Damage()
        {
            int damage = _Weapon.amount * 60;
            Console.WriteLine($"The damage is {damage}");
        }
    }
    public class GoldenGun : WeaponDecorator
    {
        public GoldenGun(Weapon Weapon) : base(Weapon) { }

        public int amount {get {return base.amount*15; } }
        public void Damage()
        {
            int damage = amount * 60;
            Console.WriteLine($"The damage is {damage}");
        }
    }
    public class GunProxy : Weapon
    {
        private Gun gun = new Gun(); 
        public int amount { get { return gun.amount; } }
        public void Damage()
        {
            gun.Damage();
        }
    }
}
